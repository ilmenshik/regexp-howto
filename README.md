# Regexp for dummies

This is short manual about how to understand regular expressions.
The main idea of this manual is to explain it as easy and short as it's possible.

## Where to start

1. Regular expression is a string with the pattern which will be applied to another string (usually for comparing or replacing).
2. This pattern can contain:
    - Just a [simple string](#simple-string) with text
    - One symbol from [range](#range) or list of symbols
    - [Equivalents](#equivalent)
    - [Group](#group) of all above and other groups
    - [Repetition](#repetition) number of all above
    - Other key-symbols (outside of this manual)
3. Most important thing you need to know about regex - some of the symbols are not are just a symbols, like dot (`.`) or asterisk (`*`) or any type of brackets, so be careful with them.


## How does it works

1. First of all you need to define some text to want to search (even it's just a one letter)
2. **Right after** your definition, optionally, you can define how much times it should be repeated to match your pattern
3. You can group parts of your text to get more control of it
4. Also you have some conditions (only for the groups) if you need to, but we can skip that for now

That's all you need to know right now.


## Entyties

### Simple string

This entity can be just a text, for example: `this what I'm looking for`
<br>And it can be just a one symbol as well: `R`, `1`, `@` or any other symbol
<br>And don't forget about empty string - it's also a string

### Range
This is a one of the most difficult parts of regexp, but all you need is just to understand how it works, don't try to memorize all of them.
<br>This entity can be:
- List of symbols, which can be defined with square brackets: `[xyz123]`
- Range of symbols also with square brackets: `[0-9]` of `[A-Z]`
- Also you can combine them like this: `[@0-9 a-zA-Z]`
  So it means: all numbers, all letters, "@" sign and the "space" sign, because of space between "9" and "a"

The only things you need to remember from here:
- All symbols inside the square brackets are in range, including space
- If you need to add "-" simbol in range you should place it before closed bracket, like this: `[0-9-]`
- And one more thing, if you need exclude defined range, you should add `^` symbold after opened bracket, like this: `[^0-9a-z]`, so it's everything except numbers and all small letters

### Equivalents
Most common equivalents are:
- `.` is stands for any simbol: letters, digits, special characters - whatever.
- `$` is stands for end of line
- `^` is stands for end


For most popular ranges we have equivalents:
- `\d` for one digit from 0 to 9
- `\w` for all latin letters
- More of these you can find here: [List of equivalents](list-of-equivalents.md)

What you need to remember from here:
- Don't be confused with dots in the end of sentences - it's not a dot, it's a special symbol for all characters

### Group
When you use circle brackets you mark you text


### Repetition
You can define how much of something can be repeated.
Do define repetition it should be placed right after symbol or group
 - Zero or one time is the question mark (`?`)
 - Zere or more times is the asterisk (`*`)
 - One or more times is the plus sign (`+`)
 - Exact numbers of repetitios looks like this: `^N`, and the `N` is stands for number, i.e.: `^5`
 - And the range of repetitions like this: `{FROM,TO}`, but here both `FROM` and `TO` are optional,
   and if not specified it's meaning there is no limit in that direction, i.e. `{2,}` meaning "two or more"

NB! Don't be confused if you are tring to repeat strings.
Examples:
- `test{2}` - will became `testt`, because `{2}` will affect only to entity before.
- And if you need to double "test", it should be like this: `(test){2}`

### Escaping

Sometimes you need to use in your text some special characters as a part of your text.
<br>That's what for we have backslash symbol: `\`

For example to create a symbol of dot instead of "any symbol": `\.`

Or backslash symbols itself should be escaped too: `\\`

## Conditions
...



a | b    union
a b    concatenation
a?    option
a*    repetition (zero or more)
a+    repetition (at least one)
a^N    repetition (exact count)
(a)    atom
[space]    ignored 

## Examples

A simple
```
```